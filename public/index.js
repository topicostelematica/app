function compare(a,b) {
  if (a.count > b.count)
    return -1;
  if (a.count < b.count)
    return 1;
  return 0;
}

$("#buscarPalabra").click(function() {
    $("#documents").remove();
    $("#documentsContainer").append('<div id="documents"></div>');
   $.ajax({
        type: "POST",
        url: "http://10.131.137.179:8080/palabra",
        data: JSON.stringify({ palabra: $("#palabra").val().toLowerCase()}),
        dataType: 'json',
        contentType: 'application/json'
    }).done(function(data) {
        console.log(data);
        console.log(data[0].docs);

        if(data){
            for(var j = 0; j<data.length; j++){
                data[j].docs.sort(compare);
            }
            for(var j = 0; j<data.length; j++){
                $("#documents").append("<h2>" + data[j].word + "</h2>");
                $("#documents").append('<ul id="documents'+ j + '"></ul>');
                for(var i = 0; i<data[j].docs.length; i++){
                    var document = "#documents" + j
                    $(document).append("<li>"+ data[j].docs[i].file +": "+ data[j].docs[i].count +"</li>");
                }
            }

        }else{
            alert("No documents");
        }

  });
})