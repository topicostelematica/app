var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var wordSchema = new Schema({
  _id: String,
  word: String,
  docs: [{
    file: String,
    count: Number
  }]
}, {collection: 'wordcount'});

var Words = mongoose.model('wordcount', wordSchema);

module.exports = Words;