var express = require('express');
var app = express();
var mongoose = require('mongoose');
var config = require('./config');
// var setupController = require('./controllers/setupController');
var wordsController = require('./controllers/wordsController');

var port = process.env.PORT || 8080;

app.use('/assets', express.static(__dirname + '/public'));

app.set('view engine', 'ejs');

mongoose.connect(config.getDbConnectionString());
//console.log(mongoose);
wordsController(app);
app.listen(port);