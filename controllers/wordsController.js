var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var WordCount = mongoose.model('wordcount', new mongoose.Schema(), 'wordcount');
//console.log(WordCount);
module.exports = function(app) {
    
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.get('/', function(req, res){
        res.render('index');
    });

    app.post('/palabra', function(req, res){
        //console.log(req.body.palabra);
        WordCount.find({word: {'$regex':req.body.palabra}}, function(err, word){
        //WordCount.find({}, function(err, word){
            console.log(word);
            if(err) throw err
            if(word){
                res.send(word);
            }else{
                res.send(null);
            }
            
        });
    });
    
}

